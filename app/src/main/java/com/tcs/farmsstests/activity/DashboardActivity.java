package com.tcs.farmsstests.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nispok.snackbar.Snackbar;
import com.tcs.farmsstests.R;
import com.tcs.farmsstests.constant.NetworkConstant;
import com.tcs.farmsstests.global.MainApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harshdeepsingh on 07/04/17.
 */

public class DashboardActivity extends AppCompatActivity {

    @Bind(R.id.btnPhTest)
    ImageButton btnPhTest;
    @Bind(R.id.btnNTest)
    ImageButton btnNTest;
    @Bind(R.id.btnKTest)
    ImageButton btnKTest;
    @Bind(R.id.btnPTest)
    ImageButton btnPTest;
    @Bind(R.id.btnEcTest)
    ImageButton btnEcTest;
    @Bind(R.id.txtId)
    TextView txtId;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    SharedPreferences preferences;
    private MaterialDialog dialog;

    @OnClick(R.id.btnNTest)
    void nitrate() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.confirmation))
                .content(R.string.test_confirmation)
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(DashboardActivity.this, NTestActivity.class));
                        finish();
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").show();
    }

    @OnClick(R.id.btnPTest)
    void phosphate() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.confirmation))
                .content(getString(R.string.test_confirmation))
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(DashboardActivity.this, PTestActivity.class));
                        finish();
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").show();
    }

    @OnClick(R.id.btnKTest)
    void potassium() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.confirmation))
                .content(R.string.test_confirmation)
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(DashboardActivity.this, KTestActivity.class));
                        finish();
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").show();
    }

    @OnClick(R.id.btnPhTest)
    void ph() {
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(DashboardActivity.this);
        dialog.title(getString(R.string.ph_test))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(getString(R.string.ph_test_msg), null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull final MaterialDialog dialog, CharSequence input) {
                        final String data = input.toString();
                        if (data.equals("")){
                            Snackbar.with(DashboardActivity.this).text("Please Enter Some Value").show(DashboardActivity.this);
                            return;
                        }
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.PH_URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("Ph Test::", response);
                                dialog.dismiss();

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    int success = jsonObject.getInt("success");

                                    if (success == 1) {
                                        new MaterialDialog.Builder(DashboardActivity.this)
                                                .title(getString(R.string.confirmation))
                                                .content(getString(R.string.next_test_confirmation))
                                                .positiveText(getString(R.string.yes))
                                                .negativeText(getString(R.string.no))
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        dialog.dismiss();
                                                        finish();
                                                        startActivity(getIntent());
                                                    }
                                                })
                                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        preferences.edit().clear().commit();
                                                        startActivity(new Intent(DashboardActivity.this, UniqueCodeActivity.class));
                                                        finish();
                                                    }
                                                })
                                                .show();
                                    } else if (success == 0) {
                                        Snackbar.with(getApplicationContext()).text(getString(R.string.already_done)).show(DashboardActivity.this);
                                    } else {
                                        Snackbar.with(getApplicationContext()).text(getString(R.string.error_message)).show(DashboardActivity.this);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Snackbar.with(getApplicationContext()).text(getString(R.string.error_message)).show(DashboardActivity.this);
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                String id = preferences.getString("code", null);
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("id", id);
                                params.put("conc", data);
                                return params;
                            }
                        };

                        MainApplication.getInstance().addToRequestQueue(stringRequest);
                    }
                }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").show();
    }

    @OnClick(R.id.btnEcTest)
    void ec() {
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(DashboardActivity.this);
        dialog.title(getString(R.string.elec_cond))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(getString(R.string.elec_cond_msg), null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull final MaterialDialog dialog, CharSequence input) {
                        final String data = input.toString();
                        if (data.equals("")){
                            Snackbar.with(DashboardActivity.this).text("Please Enter Some Value").show(DashboardActivity.this);
                            return;
                        }
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.EC_URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("EC Test::", response);
                                dialog.dismiss();

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    int success = jsonObject.getInt("success");

                                    if (success == 1) {
                                        new MaterialDialog.Builder(DashboardActivity.this)
                                                .title(getString(R.string.confirmation))
                                                .content(getString(R.string.next_test_confirmation))
                                                .positiveText(getString(R.string.yes))
                                                .negativeText(getString(R.string.no))
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        dialog.dismiss();
                                                        finish();
                                                        startActivity(getIntent());
                                                    }
                                                })
                                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        preferences.edit().clear().commit();
                                                        startActivity(new Intent(DashboardActivity.this, UniqueCodeActivity.class));
                                                        finish();
                                                    }
                                                }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                                                .show();
                                    } else if (success == 0) {
                                        Snackbar.with(getApplicationContext()).text(getString(R.string.already_done)).show(DashboardActivity.this);
                                    } else {
                                        Snackbar.with(getApplicationContext()).text(getString(R.string.error_message)).show(DashboardActivity.this);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Snackbar.with(getApplicationContext()).text(getString(R.string.error_message)).show(DashboardActivity.this);
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                String id = preferences.getString("code", null);
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("id", id);
                                params.put("conc", data);
                                return params;
                            }
                        };

                        MainApplication.getInstance().addToRequestQueue(stringRequest);
                    }
                }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.soil_tests));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        preferences = getSharedPreferences("Farmss", Context.MODE_PRIVATE);
        checkTest(preferences.getString("code", null));
        txtId.setText(preferences.getString("code", null));
        txtId.setVisibility(View.INVISIBLE);

    }

    private void checkTest(final String code) {
        dialog = new MaterialDialog.Builder(DashboardActivity.this)
                .title(R.string.check)
                .content(R.string.please_wait)
                .progress(true, 0)
                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.CHECK_TEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Tests::", response);
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.getInt("success");
                    if (success == 1) {
                        String nitrate = jsonObject.getString("Nitrate_Conc");
                        String potassium = jsonObject.getString("Potassium_Conc");
                        String phosphate = jsonObject.getString("Phosphate_Conc");
                        String ph = jsonObject.getString("Ph_Conc");
                        String ec = jsonObject.getString("Electrical_Condunctivity");

                        if (!nitrate.equals("null")) {
                            btnNTest.setVisibility(View.INVISIBLE);
                        }
                        if (!potassium.equals("null")) {
                            btnKTest.setVisibility(View.INVISIBLE);
                        }
                        if (!phosphate.equals("null")) {
                            btnPTest.setVisibility(View.INVISIBLE);
                        }
                        if (!ph.equals("null")) {
                            btnPhTest.setVisibility(View.INVISIBLE);
                        }
                        if (!ec.equals("null")) {
                            btnEcTest.setVisibility(View.INVISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Snackbar.with(DashboardActivity.this).text(R.string.error_message).show(DashboardActivity.this);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", code);
                return params;
            }
        };
        MainApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(DashboardActivity.this)
                .title(getString(R.string.exit))
                .content(getString(R.string.exit_message))
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        preferences.edit().clear().commit();
                        ActivityCompat.finishAffinity(DashboardActivity.this);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
