package com.tcs.farmsstests.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nispok.snackbar.Snackbar;
import com.tcs.farmsstests.R;
import com.tcs.farmsstests.constant.NetworkConstant;
import com.tcs.farmsstests.global.MainApplication;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class KTestActivity extends AppCompatActivity {


    private static final String TAG = "Nitrate Test Activity";
    private static final int CAMERA_REQUEST = 1777;

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully!!");
        } else {
            Log.i(TAG, "OpenCV not loaded");
        }
    }

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnSelectImage)
    Button btnSelectImage;
    @Bind(R.id.btnClickImage)
    Button btnClickImage;
    @Bind(R.id.imgStripPicture)
    ImageView imgStripPicture;
    @Bind(R.id.txtGValue)
    TextView txtGvalue;
    @Bind(R.id.txtCorrectedGValue)
    TextView txtCorrectedGValue;
    @Bind(R.id.txtStripReading)
    TextView txtStripReading;
    @Bind(R.id.txtPotassiumConcentration)
    TextView txtPotassiumConcentration;
    @Bind(R.id.btnAnotherTest)
    Button btnAnotherTest;
    SharedPreferences preferences;
    int j = 1, deltaR, deltaG, deltaB;
    int r_ref = 116, g_ref = 130, b_ref = 125;
    double quadCoeff1 = 0.045632, linearCoeff1 = -14.731216, constant1 = 1163.700316;
    double quadCoeff2 = -0.000516, linearCoeff2 = 0.219606, constant2 = 21.178778;
    double stripReading, nitrateConc;
    File sdRoot;
    String dir;
    String fileName;
    private Mat mat;
    private MaterialDialog dialog;
    private Bitmap bmp;
    private BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                populate();
            } else {
                super.onManagerConnected(status);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_k_test);
        preferences = getSharedPreferences("Farmss", Context.MODE_PRIVATE);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.potassium_test));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnClickImage.setBackgroundResource(R.drawable.ripple_rounded);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSelectImage.setBackgroundResource(R.drawable.ripple_rounded);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnAnotherTest.setBackgroundResource(R.drawable.ripple_rounded);
        }

        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
            }
        });

        btnClickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                sdRoot = Environment.getExternalStorageDirectory();
                dir = "/DCIM/Farmss/";
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                fileName = dateFormat.format(date) + ".jpg";
                final File mkdir = new File(sdRoot, dir);
                if (!mkdir.exists()) {
                    mkdir.mkdirs();
                }
                final File file = new File(sdRoot, dir + fileName);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                Uri contentUri = Uri.fromFile(file);
                Intent mediaIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaIntent.setData(contentUri);
                KTestActivity.this.sendBroadcast(mediaIntent);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        btnAnotherTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        mat = new Mat(bitmap.getWidth(), bitmap.getHeight(), CvType.CV_8UC4);
                        Utils.bitmapToMat(bitmap, mat);
                        Bitmap bitmap1 = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                        Utils.matToBitmap(mat, bitmap1);
                        bmp = bitmap1;
                        imgStripPicture.setImageBitmap(bitmap1);
                        imageInfo();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    File file = new File(sdRoot, dir + fileName);
                    Bitmap photo = decodeSampledBitmapFromFile(file.getAbsolutePath(), 3264, 3264);
                    mat = new Mat(photo.getWidth(), photo.getHeight(), CvType.CV_8UC4);
                    Utils.bitmapToMat(photo, mat);
                    Bitmap picture = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mat, picture);
                    bmp = picture;
                    imgStripPicture.setImageBitmap(picture);
                    //imgPicture.setRotation(90);
                    imageInfo();
                }
                break;
        }
    }

    private void imageInfo() {

        double lR1 = 0, lG1 = 0, lB1 = 0;
        double[] lVal1 = {};
        for (int i = 1725; i <= 1775; i++) {
            for (int j = 775; j <= 825; j++) {
                lVal1 = mat.get(i, j);
                lR1 += lVal1[0];
                lG1 += lVal1[1];
                lB1 += lVal1[2];
            }
        }
        int lR1_avg = (int) (lR1 / 2601);
        int lG1_avg = (int) (lG1 / 2601);
        int lB1_avg = (int) (lB1 / 2601);

        deltaR = lR1_avg - r_ref;
        deltaG = lG1_avg - g_ref;
        deltaB = lB1_avg - b_ref;

        double r = 0, g = 0, b = 0;
        double[] val = {};
        for (int i = 2110; i <= 2230; i++) {
            for (int j = 1135; j <= 1255; j++) {
                val = mat.get(i, j);
                r += val[0];
                g += val[1];
                b += val[2];
            }
        }
        int r_avg = (int) (r / 14641);
        int g_avg = (int) (g / 14641);
        int b_avg = (int) (b / 14641);

        int r_n_final = r_avg - deltaR;
        int g_n_final = g_avg - deltaG;
        int b_n_final = b_avg - deltaB;

        txtGvalue.setText(getResources().getString(R.string.strip_g_value) + g_avg);
        txtCorrectedGValue.setText(getResources().getString(R.string.corrected_g_value) + g_n_final);

        stripReading = (quadCoeff1 * g_n_final * g_n_final) + (linearCoeff1 * g_n_final) + constant1;
        txtStripReading.setText(getResources().getString(R.string.strip_reading) + stripReading);

        nitrateConc = (quadCoeff2 * stripReading * stripReading) + (linearCoeff2 * stripReading) + constant2;
        txtPotassiumConcentration.setText(getResources().getString(R.string.nitrate_concentration) + nitrateConc);
        Double d = Math.round(nitrateConc * 100) / 100.0d;
    }

    private Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    private void uploadImage() {
        dialog = new MaterialDialog.Builder(KTestActivity.this)
                .title(R.string.upload)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstant.POTASSIUM_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Ktest:::", response);
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.getInt("success");
                    if (success == 1) {
                        new MaterialDialog.Builder(KTestActivity.this)
                                .title(getString(R.string.confirmation))
                                .content(getString(R.string.next_test_confirmation))
                                .positiveText(getString(R.string.yes))
                                .negativeText(getString(R.string.no))
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startActivity(new Intent(KTestActivity.this, DashboardActivity.class));
                                        finish();
                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        preferences.edit().clear().commit();
                                        startActivity(new Intent(KTestActivity.this, UniqueCodeActivity.class));
                                        finish();
                                    }
                                }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                                .show();
                    } else if (success == 0) {
                        Snackbar.with(getApplicationContext()).text(getString(R.string.already_done)).show(KTestActivity.this);
                    } else {
                        Snackbar.with(getApplicationContext()).text(getString(R.string.error_message)).show(KTestActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error:::", error.getMessage().toString());
                Snackbar.with(getApplicationContext()).text(error.getMessage()).show(KTestActivity.this);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String imgNitrate = getStringImage(bmp);
                String id = preferences.getString("code", null);
                Map<String, String> params = new Hashtable<String, String>();
                params.put("id", id);
                params.put("image", imgNitrate);
                params.put("conc", String.valueOf(nitrateConc));
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainApplication.getInstance().addToRequestQueue(stringRequest, "json_obj_req");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, baseLoaderCallback);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(KTestActivity.this)
                .title(R.string.alert)
                .content(R.string.back_alert)
                .positiveText(R.string.back)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(KTestActivity.this, DashboardActivity.class));
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
