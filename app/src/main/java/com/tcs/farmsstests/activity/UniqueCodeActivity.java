package com.tcs.farmsstests.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nispok.snackbar.Snackbar;
import com.tcs.farmsstests.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.philio.pinentry.PinEntryView;

/**
 * Created by harshdeepsingh on 07/04/17.
 */

public class UniqueCodeActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnEnter)
    Button btnEnter;
    @Bind(R.id.pin_entry_simple)
    EditText pinEntryView;
    SharedPreferences preferences;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unique_code);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE}, 1);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        preferences = getSharedPreferences("Farmss", Context.MODE_PRIVATE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnEnter.setBackgroundResource(R.drawable.ripple_rounded);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinEntryView.getText().toString().equals("")) {
                    Snackbar.with(getApplicationContext()).text(getString(R.string.code_alert)).show(UniqueCodeActivity.this);
                    return;
                } else {
                    preferences.edit().putString("code", pinEntryView.getText().toString()).commit();
                    startActivity(new Intent(UniqueCodeActivity.this, DashboardActivity.class));
                    finish();
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, getString(R.string.permissions_alert), Toast.LENGTH_LONG).show();
                    ActivityCompat.finishAffinity(this);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(UniqueCodeActivity.this)
                .title(getString(R.string.exit))
                .content(getString(R.string.exit_message))
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        preferences.edit().clear().commit();
                        ActivityCompat.finishAffinity(UniqueCodeActivity.this);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                .show();
    }
}
