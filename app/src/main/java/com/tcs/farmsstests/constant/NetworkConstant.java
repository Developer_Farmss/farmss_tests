package com.tcs.farmsstests.constant;

/**
 * Created by harshdeepsingh on 11/04/17.
 */

public interface NetworkConstant {
    String GET_NETWORK_IP = "http://homephp.hol.es";
    String CHECK_TEST_URL = GET_NETWORK_IP + "/gtests.php";
    String NITRATE_URL = GET_NETWORK_IP + "/nitrate.php";
    String PHOSPHATE_URL = GET_NETWORK_IP + "/phosphate.php";
    String POTASSIUM_URL = GET_NETWORK_IP + "/potassium.php";
    String PH_URL = GET_NETWORK_IP + "/ph.php";
    String EC_URL = GET_NETWORK_IP + "/ec.php";
}
